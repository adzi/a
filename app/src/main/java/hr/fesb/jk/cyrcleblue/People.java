package hr.fesb.jk.cyrcleblue;

import java.lang.reflect.Array;

public class People {
    public String name;
    public String height;
    public String mass;
    public String hair_color;
    public String skin_color;
    public String eye_color;
    public String birth_year;
    public String gender;
    public String homeworld;
    public Array films;
    public Array species;
    public Array vehicles;
    public Array starships;
    public String url;
    public String created;
    public String edited;


}
