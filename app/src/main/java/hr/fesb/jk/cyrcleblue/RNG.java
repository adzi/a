package hr.fesb.jk.cyrcleblue;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.Random;

public class RNG extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rng_layout);
        final int[] randomnumber = new int[1];
        TextView rngtext = findViewById(R.id.rngtext);
        Button rnggenerator=findViewById(R.id.rnggrn);
        Button back=findViewById(R.id.back);
        final SharedPreferences mySharedPreferences = this.getSharedPreferences(
                "language",
                Context.MODE_PRIVATE
        );
        final int language_select=mySharedPreferences.getInt("language_sel",0);
        if(language_select==2){
            back.setText("nazad");
            rnggenerator.setText("SGB");

        }
        Bundle extras = getIntent().getExtras();
        String value = null;
        if (extras != null) {
            value = (extras.getString("value"));
            extras.clear();
        }
        rngtext.setText(value);
        rnggenerator.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                randomnumber[0] =new Random().nextInt(90)+10;
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent=new Intent(RNG.this,MainActivity.class);
                int rngvalue=randomnumber[0];
                myIntent.putExtra("RNG", rngvalue);
                startActivity(myIntent);
            }
        });
    }}
