package hr.fesb.jk.cyrcleblue;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.gson.Gson;
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final Button language=findViewById(R.id.language);
        Button login=findViewById(R.id.login);
        Button rng=findViewById(R.id.rng);
        final Spinner select=findViewById(R.id.select);
       select.setVisibility(View.GONE);

        final SharedPreferences mySharedPreferences = this.getSharedPreferences(
                "language",
                Context.MODE_PRIVATE
        );
        final int language_select=mySharedPreferences.getInt("language_sel",0);
        if ((language_select==0)){
            language.setText("-");
            login.setText("Login");
            rng.setText("RNG");
            DataStorage.character="name";
            DataStorage.height="height";
            DataStorage.mass="mass";
        }
        if ((language_select==1)){
            language.setText("english");
            login.setText("Login");
            rng.setText("RNG");
            DataStorage.character="name";
            DataStorage.height="height";
            DataStorage.mass="mass";}
        if(language_select==2){
            language.setText("hrvatski");
            login.setText("Prijava");
            rng.setText("SGB");
            DataStorage.character="ime";
            DataStorage.height="visina";
            DataStorage.mass="tezina";
        }

        final boolean[] validated = {false};
        final EditText inputtext=findViewById(R.id.inputtext);
        Bundle extras = getIntent().getExtras();
        int rng_value;
        if (extras != null) {
            rng_value = (extras.getInt("RNG"));
            extras.clear();
            inputtext.setText(String.valueOf(rng_value));
        }

        language.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                select.setClickable(true);
                select.setVisibility(View.VISIBLE);
            }
        }
);

        //select.setSelection(language_select);
        select.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position!=0){
                mySharedPreferences.edit().putInt("language_sel",position).apply();
                finish();
                startActivity(getIntent());

            }}




            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             validate();


            }
        });

        rng.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent= new Intent(MainActivity.this, RNG.class);
                String input=inputtext.getText().toString();
                myIntent.putExtra("value",input);
                startActivity(myIntent);
            }
        });
    }

    private void validate() {
        Thread thread;
        new Thread(new Runnable() {
            @Override
            public void run() {
                Button language = findViewById(R.id.language);
                Button login = findViewById(R.id.login);
                Button rng = findViewById(R.id.rng);
                language.setClickable(false);
                login.setClickable(false);
                rng.setClickable(false);
                boolean validated=false;
                EditText inputtext = findViewById(R.id.inputtext);
                String input = inputtext.getText().toString();
                if ((input.length()>4)&&(input.length()<14)){
                    if (input.contains("7")){
                       input= input.toUpperCase();
                       int c=0;
                       for (int i=0;i<input.length();i++){
                           if(input.charAt(i)=='A')c++;
                       }
                       if(c>=2){
                           if (!input.contains("?")){
                               if(input.contains("B")){
                                   for (int i=0;i<input.length();i++){
                                       if (input.charAt(i)=='B'){

                                           Log.i("circle",String.valueOf(input.charAt(i-1)));
                                           boolean check=checknumber(i-1,input);
                                           if (check==false){validated=false;break;}
                                           else validated=true;

                                       }


                                   }

                                }
                               else validated=true;


                           }

                       }


                    }


                }
                final boolean finalValidated = validated;
                runOnUiThread(new Runnable() {
    @Override
    public void run() {
        //Toast.makeText(getApplicationContext(), String.valueOf(finalValidated),Toast.LENGTH_LONG).show();
        Button language = findViewById(R.id.language);
        Button login = findViewById(R.id.login);
        Button rng = findViewById(R.id.rng);
        language.setClickable(true);
        login.setClickable(true);
        rng.setClickable(true);
        EditText inputtext=findViewById(R.id.inputtext);
        //inputtext.setText(String.valueOf(finalValidated));
        if (finalValidated==true){
            Intent myIntent=new Intent(MainActivity.this, MainScreen.class);
            startActivity(myIntent);



        }

    }
});


            }

            private boolean checknumber(int i,String input) {
                boolean validated;
                try{
                    int r= Integer.parseInt(String.valueOf(input.charAt(i)));
                    Log.i("circle",String.valueOf(r));
                    validated=false;
                }

                catch (Exception e) {
                    validated=true;
                }
                return  validated;
            }
        }).start();
    }}
