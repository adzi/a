package hr.fesb.jk.cyrcleblue;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.DatePicker;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainScreenAdapter extends BaseAdapter {
    Context mContext;
    LayoutInflater mLayoutInflater;
    private ArrayList<Frame.ResultsBean> mperson;



    public MainScreenAdapter(Context mContext,ArrayList<Frame.ResultsBean> mperson){
        this.mContext=mContext;
        this.mperson=mperson;
        this.mLayoutInflater=(LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount() {
        return mperson.size();
        //return 0;
    }

    @Override
    public Object getItem(int position) {
        return mperson.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {

        if (view==null){view=mLayoutInflater.inflate(R.layout.mainscreenadapterfirstitem,parent,false);}
            TextView name=view.findViewById(R.id.name);
            TextView height=view.findViewById(R.id.height);
            TextView mass=view.findViewById(R.id.mass);
            Frame.ResultsBean item=(Frame.ResultsBean) getItem(position);
            name.setText(DataStorage.character+"  "+item.getName());
           height.setText(DataStorage.height+"  "+String.valueOf(item.getHeight()));
            mass.setText(DataStorage.mass+"   "+String.valueOf(item.getMass()));

notifyDataSetChanged();
        return  view;}
    }

